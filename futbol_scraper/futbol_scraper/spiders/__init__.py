import scrapy

class LigaMxSpider(scrapy.Spider):
    name = "liga_mx"
    allowed_domains = ["ligamx.net"]  
    start_urls = [
        'https://ligamx.net/cancha/estadisticahistorica'  # Reemplaza con la URL de los resultados de la Liga MX
    ]

    def parse(self, response):
        teams = response.xpath('//table[@class="default tbl_grals"]/tbody/tr')
        # .//table[@class="default tbl_grals"]/tbody/tr/td[5]
        for team in teams[:]: 
            yield {
                'club': team.xpath('normalize-space(.//td/a[@class="tpts loadershow"]/text())').get(),
                'jj': team.xpath('.//td[3]/text() | .//td[3]/a/text()').get(),
                'jg': team.xpath('.//td[4]/text() | .//td[4]/a/text()').get(),
                'je': team.xpath('.//td[5]/text() | .//td[5]/a/text()').get(),
                'jp': team.xpath('.//td[6]/text() | .//td[6]/a/text()').get(),
                'gf': team.xpath('.//td[7]/text() | .//td[7]/a/text()').get(),
                'gc': team.xpath('.//td[8]/text() | .//td[8]/a/text()').get(),
                'dif': team.xpath('.//td[9]/text() | .//td[9]/a/text()').get(),
                'pts': team.xpath('.//td[10]/text() | .//td[10]/a/text()').get(),
                'jjl': team.xpath('.//td[11]/text() | .//td[11]/a/text()').get(),
                'jgl': team.xpath('.//td[12]/text() | .//td[12]/a/text()').get(),
                'jel': team.xpath('.//td[13]/text() | .//td[13]/a/text()').get(),
                'jpl': team.xpath('.//td[14]/text() | .//td[14]/a/text()').get(),
                'gfl': team.xpath('.//td[15]/text() | .//td[15]/a/text()').get(),
                'gcl': team.xpath('.//td[16]/text() | .//td[16]/a/text()').get(),
                'difl': team.xpath('.//td[17]/text() | .//td[17]/a/text()').get(),
                'ptsl': team.xpath('.//td[18]/text() | .//td[18]/a/text()').get(),
                'jjv': team.xpath('.//td[19]/text() | .//td[19]/a/text()').get(),
                'jgv': team.xpath('.//td[20]/text() | .//td[20]/a/text()').get(),
                'jev': team.xpath('.//td[21]/text() | .//td[21]/a/text()').get(),
                'jpv': team.xpath('.//td[22]/text() | .//td[22]/a/text()').get(),
                'gfv': team.xpath('.//td[23]/text() | .//td[23]/a/text()').get(),
                'gcv': team.xpath('.//td[24]/text() | .//td[24]/a/text()').get(),
                'difv': team.xpath('.//td[24]/text() | .//td[24]/a/text()').get(),
                'ptsv': team.xpath('.//td[24]/text() | .//td[24]/a/text()').get(),
                
            }

        # next_page = response.xpath('//a[@class="siguiente-pagina"]/@href').get()
        # if next_page:
        #     yield response.follow(next_page, self.parse)
