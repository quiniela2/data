# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
# from itemadapter import ItemAdapter


# class FutbolScraperPipeline:
#     def process_item(self, item, spider):
#         return item

import csv

class FutbolScraperPipeline:

    def open_spider(self, spider):
        self.file = open('liga_mx.csv', 'w', newline='')
        self.writer = csv.writer(self.file)
        self.writer.writerow(['Club','Juegos jugados', 'Juegos Ganados', 'Juegos Empatados', 'Juegos Perdidos', 'Goles a Favor', 'Goles en contra', 'Diferencia', 'Puntos',
                            'Juegos jugados Local', 'Juegos Ganados Local', 'Juegos Empatados Local', 'Juegos Perdidos Local', 'Goles a Favor Local', 'Goles en contra Local', 'Diferencia Local', 'Puntos Local',
                            'Juegos jugados Visitante', 'Juegos Ganados Visitante', 'Juegos Empatados Visitante', 'Juegos Perdidos Visitante', 'Goles a Favor Visitante', 'Goles en contra Visitante', 'Diferencia Visitante', 'Puntos Visitante'])

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        self.writer.writerow([item ['club'], item['jj'], item['jg'], item['je'], item['jp'], item['gf'], item['gc'],item['dif'], item['pts'],
                            item['jjl'], item['jgl'], item['jel'], item['jpl'], item['gfl'], item['gcl'],item['difl'], item['ptsl'], 
                            item['jjv'], item['jgv'], item['jev'], item['jpv'], item['gfv'], item['gcv'],item['difv'], item['ptsv'], ])
        return item
