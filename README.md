# Scraper de Resultados de la Liga MX con Scrapy

Este proyecto de Scrapy se utiliza para extraer los resultados de los partidos de la [Liga MX](https://www.ligamx.net/). El proyecto está configurado para facilitar la expansión y permitir la extracción de datos de otras ligas de fútbol en el futuro.

## Requisitos

- Python 3.x
- Scrapy

## Instalación

1. **Clona el repositorio:**

   ```bash
   git clone https://gitlab.com/quiniela2/data.git
   cd data
